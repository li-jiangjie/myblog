export default {
  index: [
    {
      icon: 'el-icon-edit-outline',
      route: 'edit'
    }
  ],
  article: [
    {
      icon: 'el-icon-edit-outline',
      route: 'edit',
      like: true
    },
    {
      icon: 'el-icon-chat-dot-square',
      method: 'focusTextarea'

    }
  ],
  colunm: [
    {
      icon: 'el-icon-plus',
      method: 'addColumn'
    }
  ]
}
