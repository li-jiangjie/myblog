export default {
  login: {
    title: '登录',
    formtype: 'login',
    btns: [{
      btntype: 'button',
      type: 'close',
      name: '取消'
    }, {
      btntype: 'submit',
      type: 'confirm',
      name: '登录',
      isSubmit: true
    }]
  },
  columns: {
    title: '分类添加',
    formtype: 'postcolumn',
    needupdata: true,
    btns: [{
      btntype: 'button',
      type: 'close',
      name: '取消'
    }, {
      btntype: 'columns',
      type: 'confirm',
      name: '添加',
      isSubmit: true
    }]
  },
  info: {
    formtype: 'putinfo',
    btns: [{
      btntype: 'submit',
      type: 'info',
      name: '修改',
      isSubmit: true
    }]
  },
  register: {
    title: '注册',
    formtype: 'register',
    btns: [{
      btntype: 'button',
      type: 'close',
      name: '取消'
    }, {
      btntype: 'submit',
      type: 'confirm',
      name: '注册',
      isSubmit: true
    }]
  }
}
