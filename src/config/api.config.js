export default {
  register: {
    url: '/admin/register',
    pwdrsa: 'password',
    method: 'post',
    setToken: true
  },
  postArticle: {
    url: '/api/rest/articles',
    method: 'post'
  },
  getArticleByID: {
    url: '/api/rest/articles/:id',
    rest: true,
    method: 'get'
  },
  user: {
    url: '/index',
    noMsg: true,
    method: 'get'
  },
  getinfo: {
    url: '/user',
    method: 'get'
  },
  putinfo: {
    url: '/user',
    method: 'put'
  },
  login: {
    url: '/admin/login',
    pwdrsa: 'password',
    method: 'post',
    setToken: true
  },
  pubkey: {
    url: '/keys',
    method: 'get'
  },
  getAllArticle: {
    url: '/api/rest/articles',
    method: 'get'
  },
  columns: {
    url: '/api/rest/columns',
    method: 'get'
  },
  postcolumn: {
    url: '/api/rest/columns',
    method: 'post'
  },
  postcomment: {
    url: '/api/rest/comment',
    method: 'post'
  },
  putarticleLikes: {
    url: '/articles/likes/:id',
    method: 'post',
    rest: true
  }
}
