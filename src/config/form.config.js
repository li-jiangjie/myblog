export default {
  login: [{
    label: '用户名',
    type: 'text',
    placeholder: '用户名: 6-12位 字母数字',
    name: 'username'
  }, {
    label: '密码',
    type: 'password',
    placeholder: '密码: 6-12位 最少包含一位(数字/大小写字母)',
    name: 'password'
  }],
  columns: [{
    label: '文章分类',
    type: 'text',
    placeholder: '请填写文章分类',
    name: 'name'
  }],
  info: [{
    label: '头像',
    placeholder: '上传头像',
    name: 'avatar'
  },
  {
    readonly: true,
    label: '用户名',
    type: 'text',
    placeholder: '用户名: 6-12位 字母数字',
    name: 'username'
  },
  {
    label: '邮箱',
    type: 'text',
    placeholder: '修改你的邮箱',
    name: 'email'
  },
  {
    label: '昵称',
    type: 'text',
    placeholder: '修改你的昵称',
    name: 'nikname'
  },
  {
    label: '签名',
    type: 'text',
    placeholder: '修改你的个性签名',
    name: 'signature'
  }
  ],
  register: [{
    label: '用户名',
    type: 'text',
    placeholder: '用户名: 6-12位 字母数字',
    name: 'username'
  }, {
    label: '密码',
    type: 'password',
    placeholder: '密码: 6-12位 最少包含一位(数字/大小写字母)',
    name: 'password'
  }, {
    label: '邮箱',
    type: 'text',
    placeholder: '请填写正确的邮箱格式',
    name: 'email'
  }
  ]
}
