export default {
  username: [
    { required: true, message: '请输入用户名', trigger: 'blur' },
    { min: 6, max: 12, message: '长度在 6 到 12 个字符', trigger: 'blur' }
  ],
  password: [
    { required: true, message: '请输入密码', trigger: 'blur' },
    { pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d!.#*?&]{8,12}$/, message: '密码格式 至少包含大写字母+小写字母+数字 8-12位', trigger: 'blur' }
  ],
  email: [
    { type: 'email', required: true, message: '请输入正确的邮箱地址', trigger: ['blur', 'change'] }
  ],
  name: [
    { required: true, message: '分类必填', trigger: 'blur' }
  ],
  nikname: [
    { required: true, message: '昵称必填', trigger: 'blur' }
  ]
}
