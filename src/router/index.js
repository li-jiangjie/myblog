import Vue from 'vue'
import VueRouter from 'vue-router'
import ArticleList from '@/views/ArticleListView.vue'
import Article from '@/views/ArticleView.vue'
import Colunm from '@/views/ColunmView.vue'
import Home from '@/views/HomeView.vue'
import User from '@/views/userView.vue'
import Socket from '@/views/SocketView.vue'
import Edit from '@/views/EditView.vue'
import store from '@/store'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    redirect: '/index',
    component: Home,
    children: [
      { path: '/index', name: 'index', component: ArticleList },
      {
        path: '/colunm',
        name: 'colunm',
        component: Colunm,
        meta: {
          requiresAuth: true
        }
      },
      { path: '/article/:id', name: 'article', component: Article },
      {
        path: '/user',
        name: 'user',
        component: User,
        meta: {
          requiresAuth: true
        }
      },
      { path: '/socket', name: 'socket', component: Socket },
      { path: '/edit', name: 'edit', component: Edit }
    ]
  }
]

const router = new VueRouter({
  routes
})
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}

router.beforeEach((to, from, next) => {
  console.log(111)
  if (to.meta.requiresAuth && !store.state.token) {
    Vue.prototype.$notify({
      title: '警告',
      message: '需要登入才能进入',
      type: 'warning'
    })
    if (from.path === to.path) {
      console.log('提示: 已经在个路径里了')
    } else {
      next('/index')
    }
  }
  next()
})
export default router
