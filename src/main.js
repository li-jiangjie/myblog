import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './plugins/element.js'
import './css/global.styl'
import 'element-ui/lib/theme-chalk/display.css'
import http from '@/plugins/http'
import '@/plugins/vuescroll'
import loadsh from 'loadsh'

Vue.config.productionTip = false
Vue.prototype.$EventBus = new Vue()
Vue.use(http)
new Vue({
  router,
  loadsh,
  store,
  render: h => h(App)
}).$mount('#app')
