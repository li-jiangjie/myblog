import Vue from 'vue'
import Vuex from 'vuex'
import model from './modules/model'
import store from 'store'
import http from '@/api/http'
import base from '@/config/base.config'
import like from './modules/like'
import { io } from 'socket.io-client'
Vue.use(Vuex)
const { TOKEN_NAME } = base
export default new Vuex.Store({
  state: {
    token: store.get(TOKEN_NAME) || '',
    userInfo: {},
    accident: false
  },
  getters: {
    getUserInfo (state) {
      return state.userInfo
    }
  },
  mutations: {
    SET_TOKEN (state) {
      state.token = store.get(TOKEN_NAME)
    },
    CANCLE_TOKEN (state) {
      state.token = ''
      store.remove(TOKEN_NAME)
      state.userInfo = {}
      // Vue.prototype.$ws.emit('logout')
    },
    SET_USERINFO (state, userInfo) {
      state.userInfo = userInfo
    }
  },
  actions: {
    async login ({ dispatch, commit }) {
      commit('SET_TOKEN')
      dispatch('getUserInfo')
    },
    async logout ({ dispatch, commit, state }) {
      Vue.prototype.$ws.close()

      commit('CANCLE_TOKEN')
    },
    online ({ commit, state }) {
      Vue.prototype.$ws = io('ws://127.0.0.1:8888', { transports: ['websocket'] })
      const info = state.userInfo
      console.log(info)
      Vue.prototype.$ws.emit('login', { uid: info._id, nikname: info.nikname })
      Vue.prototype.$ws.once('accidentout', () => {
        state.accident = true
      })
      Vue.prototype.$ws.once('disconnect', () => {
        const msg = state.accident ? '您已在其他设备登陆' : `退出成功,再见 ${state.userInfo.nikname}`
        Vue.prototype.$notify({
          title: '提示',
          message: msg,
          type: 'info'
        })
        state.accident = false
        commit('CANCLE_TOKEN')
      })
    },
    async getUserInfo ({ dispatch, commit }) {
      try {
        const userInfo = await http({ type: 'getinfo' })
        commit('SET_USERINFO', userInfo)
        dispatch('online')
        Vue.prototype.$notify({
          title: '成功',
          message: `欢迎你${userInfo.nikname}`,
          type: 'success'
        })
      } catch (err) {
        console.log(err)
      }
    }
  },
  modules: {
    model, like
  }
})
