import store from 'store'
export default {
  namespaced: true,
  state: {
    likes: store.get('userLikes') || [],
    isLike: true
  },
  getters: {
    isLike (state) {
      return state.isLike
    }
  },
  mutations: {
    GETLIKE (state) {
      state.likes = store.get('userLikes')
    },
    ISLIKE (state, isLike) {
      console.log(isLike)
      state.isLike = isLike
    }
  },
  actions: {
    pushLike (context, payload) {
      const { aid } = payload
      console.log(aid + 'kkkk')
      if (aid) {
        const userLikes = store.get('userLikes') || []
        userLikes.push(aid)
        store.set('userLikes', userLikes)
        context.commit('GETLIKE')
      }
    },
    pullLike (context, { aid }) {
      if (aid) {
        const userLikes = store.get('userLikes') || []
        const aidIndex = userLikes.findIndex((item) => {
          return item === aid
        })
        userLikes.splice(aidIndex, 1)
        store.set('userLikes', userLikes)
        context.commit('GETLIKE')
      }
    },
    changeIsLike (context, { isLike }) {
      context.commit('ISLIKE', isLike)
    }
  }
}
