export default {
  namespaced: true,
  state: {
    isShow: false,
    type: ''
  },
  mutations: {
    close (state) {
      state.isShow = false
    },
    open (state) {
      state.isShow = true
    },
    change_type (state, playload) {
      state.type = playload.type
    }
  },
  actions: {
    close (context) {
      context.commit('close')
    },
    open (context, { type }) {
      context.commit('change_type', { type })
      context.commit('open')
    },
    confirm () {

    }
  },
  modules: {

  }
}
