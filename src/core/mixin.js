export default {
  methods: {
    refreshModel (type) {
      this.$store.dispatch('model/open', { type })
    }
  }
}
