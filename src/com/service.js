import axios from 'axios'
import store from 'store'
import base from '@/config/base.config'
const { TOKEN_NAME } = base
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
  timeout: 5000
})
service.interceptors.request.use(async (config) => {
  const token = store.get(TOKEN_NAME)
  if (token) {
    config.headers['Authorization'] = `Bearer ${token}`
  }
  return config
}, (err) => {
  return Promise.reject(err)
})
service.interceptors.response.use((response) => {
  const { data: result } = response
  return result
}, (err) => {
  return Promise.reject(err)
})
export default service
