import vuescroll from 'vuescroll'
import Vue from 'vue'
Vue.use(vuescroll, {
  ops: {
    bar: {
      showDelay: 500,
      onlyShowBarOnScroll: true,
      keepShow: false,
      background: '#c1c1c1',
      opacity: 1,
      hoverStyle: false,
      specifyBorderRadius: false,
      minSize: 0.3,
      size: '6px',
      disable: false
    }
  },
  name: 'Scroll'
})
