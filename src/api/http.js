import encrypt from '@/util/encrypt'
import service from '@/com/service'
import RequestType from '@/config/api.config'
import store from 'store'
import base from '@/config/base.config'
export default async function http ({ type, data }) {
  if (!(type in RequestType)) {
    throw new Error('Api请求错误')
  }
  let { url, method, pwdrsa = false, setToken = false, rest = false, noMsg = false } = RequestType[type]
  if (pwdrsa && data[pwdrsa]) {
    data[pwdrsa] = await encrypt(data[pwdrsa])
  }
  if (rest) {
    url = url.replace(/:id/, data.id)
  }
  data = method === 'get' ? { params: data } : data
  try {
    const { data: result } = await service[method](url, data)
    if (setToken) {
      store.set(base.TOKEN_NAME, result.token)
      this.$store.dispatch('login')
    }
    return result
  } catch (err) {
    if (!noMsg) {
      const errMessage = err.response.data.message
      this.$notify({
        title: '错误',
        message: errMessage,
        type: 'error'
      })
      console.log(errMessage)
      return Promise.reject(err)
    }
    return Promise.reject(err)
  }
}
