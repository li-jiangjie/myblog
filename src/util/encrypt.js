import store from 'store'
import JSEncrypt from 'jsencrypt'
import base from '@/config/base.config'
import service from '@/com/service'

const { Pubkey_NAME } = base
export default async function encrypt (value) {
  let key = store.get(Pubkey_NAME)
  if (!key || key === 'undefined') {
    try {
      const { data: result } = await service.get('/keys')
      key = result.pubKey
      key = key.replace(/\. +/g, '')
      key = key.replace(/[\r\n]/g, '')
      store.set(Pubkey_NAME, key)
    } catch (err) {
      console.log(err)
    }
  }
  const encryptKey = new JSEncrypt()
  encryptKey.setPublicKey(key)
  return encryptKey.encrypt(value)
}
